﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    [SerializeField] private SpawnWorld spawnWorld;
    private Player player;

    public Player Player
    { 
        set
        {
            player = value;
        }
    }

    private PlayerController inputs;


    private void OnEnable()
    {
        inputs.Enable();
    }

    private void OnDisable()
    {
        inputs.Disable();
    }

    private void PlayerMove(Player.MoveDirection direction)
    {
        if(player.position.y == player.StartY && direction == Player.MoveDirection.Down)
        {
            spawnWorld.MoveDown();
        }
        else
        {
            player.PlayerMove(direction);
        }
        if(spawnWorld.GetOre(player.position) == Ore.OreType.Ground)
        {
            Debug.Log("Ground");
        }
    }

    void Awake()
    {

        inputs = new PlayerController();
        inputs.Player.MoveDown.performed += context => PlayerMove(Player.MoveDirection.Down);
        inputs.Player.MoveUp.performed += context => PlayerMove(Player.MoveDirection.Up);
        inputs.Player.MoveLeft.performed += context => PlayerMove(Player.MoveDirection.Left);
        inputs.Player.MoveRight.performed += context => PlayerMove(Player.MoveDirection.Right);
    }
}
