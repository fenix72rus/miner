﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Ore : Block
{
    public OreType oreType { get; set; }

    public enum OreType
    {
        Ground,
        Stone,
        Coal,
        Iron,
        Gold
    }

    public Ore(Sprite sprite, Vector2 position, GameObject block_prefab, OreType ore)
    {
        _gameObject = block_prefab;
        oreType = ore;

        Draw(sprite, position, block_prefab, 1);
    }

    public void ChangeOre(Sprite sprite, OreType ore)
    {
        oreType = ore;

        Draw(sprite);
    }
}
