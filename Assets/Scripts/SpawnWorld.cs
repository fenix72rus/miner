﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnWorld : MonoBehaviour
{
    [SerializeField] private GameObject block_prefab;
    [SerializeField] private Sprite ground_sprite, top_ground_sprite, stone_sprite, player_sprite;
    [SerializeField] private Move move;

    [SerializeField] private int depth;
    private static int block_count_x = 11, block_count_y = 21;
    private Block[,] blocks = new Block[block_count_x, block_count_y];

    private void Awake()
    {
        for (int y = 0; y < block_count_y; y++)
        {
            for (int x = 0; x < block_count_x; x++)
            {
                if (y == 0)
                {
                    Ore ore = new Ore(top_ground_sprite, new Vector2(-block_count_x / 2 + x, -y), block_prefab, Ore.OreType.Ground);
                    blocks[x, y] = (Block)ore;
                }
                else
                {
                    Ore ore = new Ore(ground_sprite, new Vector2(-block_count_x / 2 + x, -y), block_prefab, Ore.OreType.Ground);
                    blocks[x, y] = (Block)ore;
                }
            }
        }

        move.Player = new Player(player_sprite, new Vector2(0, 1), block_prefab);
    }

    private void Update()
    {
    }

    public Ore.OreType GetOre(Vector2 blockPosition)
    {
        Ore ore = (Ore)blocks[(int)blockPosition.x + 6, (int)blockPosition.y];

        return ore.oreType;
    }

    public void MoveDown()
    {
        depth++;
        for (int y = 0; y < blocks.GetLength(1); y++)
        {
            for (int x = 0; x < blocks.GetLength(0); x++)
            {
                Block block = blocks[x, y];

                if (block.position.y == 10)
                {
                    block.Move(new Vector2(block.position.x, -blocks.GetLength(1) / 2));
                    int change = (depth / 10) <= 30 ? depth / 10 : 30;
                    if (Random.Range(0, 100) <= change)
                    {
                        Ore ore = (Ore)block;
                        ore.ChangeOre(stone_sprite, Ore.OreType.Stone);
                    }
                    else
                    {
                        Ore ore = (Ore)block;
                        ore.ChangeOre(ground_sprite, Ore.OreType.Ground);
                    }
                    //blocks[x, blocks.GetLength(1) - 1] = block;
                }
                else
                {
                    block.Move(new Vector2(block.position.x, block.position.y + 1));
                    //blocks[x, y + 1] = block;
                }
            }
        }
        Block[,] out_massiv = new Block[block_count_x, block_count_y];
        for (int y = 0; y < block_count_y; y++)
        {
            for (int x = 0; x < block_count_x; x++)
            {
                if (y == block_count_y - 1)
                {
                    out_massiv[x, y] = blocks[x, 0];
                }
                else
                {
                    out_massiv[x, y] = blocks[x, y + 1];
                }
            }
        }
        blocks = out_massiv;
    }
}
