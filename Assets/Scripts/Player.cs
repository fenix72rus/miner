﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Block
{
    private float startY;

    public float StartY
    { 
        get
        {
            return startY;
        }
    }

    public enum MoveDirection
    { 
        Up,
        Down,
        Left,
        Right
    }

    public Player(Sprite sprite, Vector2 _position, GameObject block_prefab)
    {
        Draw(sprite, _position, block_prefab, 2);
        startY = _position.y;
    }

    public void PlayerMove(MoveDirection direction)
    {
        switch (direction)
        {
            case MoveDirection.Down:
                Move(new Vector2(position.x, position.y - 1));
                break;
            case MoveDirection.Up:
                Move(new Vector2(position.x, position.y + 1));
                break;
            case MoveDirection.Left:
                Move(new Vector2(position.x - 1, position.y));
                break;
            case MoveDirection.Right:
                Move(new Vector2(position.x + 1, position.y));
                break;
            default:
                break;
        }
    }
}
