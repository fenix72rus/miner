﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : SpawnWorld
{
    public Vector2 position { get; set; }
    public GameObject _gameObject { get; set; }

    public void Draw(Sprite sprite, Vector2 _position, GameObject block_prefab, int sortingLayer)
    {
        _gameObject = Instantiate(block_prefab, _position, Quaternion.Euler(0, 0, 0));
        SpriteRenderer spriteRenderer = _gameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = sprite;
        spriteRenderer.sortingOrder = sortingLayer;
        position = _position;
    }

    public void Draw(Sprite sprite)
    {
        SpriteRenderer spriteRenderer = _gameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = sprite;
    }

    public void Move(Vector2 _position)
    {
        _gameObject.transform.position = _position;
        position = _position;
    }
}
